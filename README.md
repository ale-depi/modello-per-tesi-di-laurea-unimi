# Un modello per le tesi di laurea

***ATTENZIONE***

* L'Ateneo mette a disposizione degli studenti i loghi secondo l'apposita
  [pagina](https://work.unimi.it/servizi/comunicare/37094.htm).
* All'interno dei files `solo_fronte.tex` e `fronte_retro.tex` vi sono delle
  righe contenenti `% ------- da cambiare`. Il significato delle variabili da
modificare è piuttosto chiaro.
* Il *backend* per la bibliografia è *biber* e non *bibtex*. Qualora si stia
  utilizzando un editor quale [Texmaker](https://www.xm1math.net/texmaker/), si
modifichino le impostazioni.
* Ricordarsi, prima di caricare la tesi nell'archivio, di compilare il PDF
  secondo lo standard A. Per fare ciò, decommentare la riga successiva a `%
------- prima di caricare`.

## Anteprime

Di seguito vengono riportate le anteprime PDF delle versioni.

* [Solo-fronte](https://gitlab.com/ale-depi/modello-per-tesi-di-laurea-unimi/-/jobs/artifacts/master/raw/solo_fronte.pdf?job=compile_pdf)  
Il formato solo-fronte è stato pensato per l'archiviazione in formato digitale
e per la stampa su singola facciata. Risulta inoltre adatto all'archiviazione
digitale in quanto non è prevista distinzione tra l'impaginazione delle pagine
dispari e quella delle pagine pari.

* [Fronte-retro](https://gitlab.com/ale-depi/modello-per-tesi-di-laurea-unimi/-/jobs/artifacts/master/raw/fronte_retro.pdf?job=compile_pdf)  
Il formato fronte-retro è stato pensato per la stampa su entrambe le facciate,
in maniera da ottimizzare il quantitativo di carta e rendere snelle tesi
voluminose.

## Ulteriori informazioni

Per ogni altra informazione, si consulti la
[Wiki](https://gitlab.com/ale-depi/modello-per-tesi-di-laurea-unimi/-/wikis/home).
